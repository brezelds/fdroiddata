Categories:Navigation
License:MIT
Web Site:https://adamsprogs.ml/w/programmes/bimba
Source Code:https://github.com/apiote/Bimba
Issue Tracker:https://github.com/apiote/Bimba/issues

Auto Name:Bimba
Summary:First Free-Software Poznań Wandering Guide
Description:
With this app You can check the public transport timetable in Poznań
agglomeration (run by ZTM Poznań), and thanks to the Virtual Monitor You can see
when exactly a bus or tram will arrive.

More features to come (with roadmap available
[https://github.com/apiote/Bimba/blob/master/README.md#roadmap there])
.

Repo Type:git
Repo:https://github.com/apiote/Bimba

Build:1.0.2,3
    commit=v1.0.2
    subdir=app
    gradle=yes

Build:1.1.0,4
    commit=v1.1.0
    subdir=app
    gradle=yes

Auto Update Mode:Version v%v
Update Check Mode:Tags
Current Version:1.1.0
Current Version Code:4
